package com.example.demo.controller;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class ForumController {

	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top() {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport();

		// コメントを全件取得
		List<Comment> replyData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("replys", replyData);

		return mav;
	}

	// 新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		// 画面遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", report);
		return mav;
	}

//	// 投稿処理
//	@PostMapping("/add")
//	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
//		// 投稿をテーブルに格納
//		reportService.saveReport(report);
//		// rootへリダイレクト
//		return new ModelAndView("redirect:/");
//	}

//	 投稿処理 ajax
	@PostMapping("/add")
	@ResponseBody
	public String addContent(@ModelAttribute("formModel") Report report) {

		// 更新時間をセット
		Date currentDate = new Date();
		report.setCreatedDate(currentDate);
		report.setUpdatedDate(currentDate);
		// 投稿をテーブルに格納
		reportService.saveReport(report);

		// コメントを取得
		List<Report> LatestPost = reportService.findSelectedPost();

		return GetJson(LatestPost);
	}

    private String GetJson(List<Report> LatestPost){
        String retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            retVal = objectMapper.writeValueAsString(LatestPost);
        } catch (JsonProcessingException e) {
            System.err.println(e);
        }
        return retVal;
    }
 // 投稿処理 ajax

	//	// 投稿削除
	//	@DeleteMapping("/delete/{id}")
	//	public ModelAndView deleteContent(@PathVariable Integer id) {
	//		// テーブルから投稿を消去
	//		reportService.deleteReport(id);
	//		return new ModelAndView("redirect:/");
	//	}

	// 投稿削除 Ajax
	@DeleteMapping("/delete/{id}")
	@ResponseBody
	public void deleteContent(@PathVariable Integer id) {
		// テーブルから投稿を消去
		reportService.deleteReport(id);
	}

	// 編集画面
//	@GetMapping("/edit/{id}")
//	public ModelAndView editContent(@PathVariable Integer id) {
//		ModelAndView mav = new ModelAndView();
//		// 編集する投稿を取得
//		Report report = reportService.editReport(id);
//		// 編集する投稿をセット
//		mav.addObject("formModel", report);
//		// 画面遷移先を指定
//		mav.setViewName("/edit");
//		return mav;
//	}

	// Ajax 編集画面
	@GetMapping("/edit/{id}")
	@ResponseBody
	public String editContent(@PathVariable Integer id) {
		// 編集する投稿を取得
		Report report = reportService.editReport(id);

		return getEditJson(report);
	}

    private String getEditJson(Report editContent){
        String retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            retVal = objectMapper.writeValueAsString(editContent);
        } catch (JsonProcessingException e) {
            System.err.println(e);
        }
        return retVal;
    }
	// Ajax 編集画面

	// 編集処理
//	@PutMapping("/update/{id}")
//	public ModelAndView updateContent (@PathVariable Integer id, @ModelAttribute("formModel") Report report) {
//		// UrlParameterのidを更新するentityにセット
//		report.setId(id);
//
//		// 更新時間をセット
//		Date currentDate = new Date();
//		report.setUpdatedDate(currentDate);
//
//		// 編集した投稿を更新
//		reportService.saveReport(report);
//		// rootへリダイレクト
//		return new ModelAndView("redirect:/");
//	}

    // Ajax 編集機能
	@PutMapping("/updata/{id}")
	@ResponseBody
	public String updateContent (@PathVariable Integer id, @RequestParam String content) {

		Report report = new Report();

		report.setContent(content);
		report.setId(id);

		// 更新時間をセット
		Date currentDate = new Date();
		report.setUpdatedDate(currentDate);

		// 編集した投稿を更新
		reportService.saveReport(report);

		// 編集する投稿を取得
		Report updataContent = reportService.editReport(id);

		return putEditJson(updataContent);
	}

    private String putEditJson(Report editContent){
        String retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            retVal = objectMapper.writeValueAsString(editContent);
        } catch (JsonProcessingException e) {
            System.err.println(e);
        }
        return retVal;
     }
	 // Ajax 編集機能

	// コメント処理
//	@PostMapping("/comment/{id}")
//	public ModelAndView commentContent(@PathVariable Integer id, @ModelAttribute("formModel") Comment comment,  @ModelAttribute("formModel") Report report) {
//		comment.setReport_id(id);
//		// 投稿をテーブルに格納
//		commentService.saveComment(comment);
//
//		// 更新時間をセット
//		Date currentDate = new Date();
//		report.setUpdatedDate(currentDate);
//		report.setId(id);
//		reportService.updateReport(report);
//
//		// rootへリダイレクト
//		return new ModelAndView("redirect:/");
//	}

	// コメント処理 Ajax
	@PostMapping("/comment/{id}")
	@ResponseBody
	public String reply(@PathVariable Integer id,
			@RequestParam String reply,
			@RequestParam String content) {

		Comment comment = new Comment();

		comment.setReport_id(id);
		comment.setReply(reply);

		commentService.saveComment(comment);

		Report report = new Report();

		// 更新時間をセット
		Date currentDate = new Date();
		report.setUpdatedDate(currentDate);
		report.setId(id);
		report.setContent(content);

		reportService.updateReport(report);

		// コメントを全件取得
		List<Comment> LatestComment = commentService.findSelectedComment();

		return  getJson(LatestComment);
	}

    private String getJson(List<Comment> latestComment){
        String retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            retVal = objectMapper.writeValueAsString(latestComment);
        } catch (JsonProcessingException e) {
            System.err.println(e);
        }
        return retVal;
    }

 // コメント処理
//	@PostMapping("/comment/{id}")
//	@ResponseBody
//	public String reply(@PathVariable Integer id,
//			@RequestParam String reply,
//			@RequestParam String content) {
//
//		Comment comment = new Comment();
//
//		comment.setReport_id(id);
//		comment.setReply(reply);
//
//		commentService.saveComment(comment);
//
//		Report report = new Report();
//
//		// 更新時間をセット
//		Date currentDate = new Date();
//		report.setUpdatedDate(currentDate);
//		report.setId(id);
//		report.setContent(content);
//
//		reportService.updateReport(report);
//
//		return reply;
//	}

	// コメント編集画面
	@GetMapping("/commentEdit/{id}")
	public ModelAndView commenteditContent(@PathVariable Integer id) {
		ModelAndView mav = new ModelAndView();
		// 編集する投稿を取得
		Comment comment = commentService.editComment(id);
		// 編集する投稿をセット
		mav.addObject("formModel", comment);
		// 画面遷移先を指定
		mav.setViewName("/commentEdit");
		return mav;
	}


	// コメント編集処理
	@PutMapping("/commentUpdate/{id}")
	public ModelAndView commentUpdateContent (@PathVariable Integer id,Integer report_id, @ModelAttribute("formModel") Comment comment) {
		// UrlParameterのidを更新するentityにセット
		comment.setId(id);
		// UrlParameterのidを更新するentityにセット
		comment.setReport_id(report_id);
		// 編集した投稿を更新
		commentService.saveComment(comment);
		// rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	// コメント削除
	//	@DeleteMapping("/commentDelete/{id}")
	//	public ModelAndView deleteComment(@PathVariable Integer id) {
	//		// テーブルから投稿を消去
	//		commentService.deleteComment(id);
	//		return new ModelAndView("redirect:/");
	//	}

	// コメント削除 Ajax
	@DeleteMapping("/commentDelete/{id}")
	@ResponseBody
	public void deleteComment(@PathVariable Integer id) {
		// テーブルから投稿を消去
		commentService.deleteComment(id);
	}

//	// 投稿の絞り込み
//	@GetMapping("/narrow")
//	public ModelAndView narrowContent(@RequestParam(name="startDate") String startDate,
//			@RequestParam(name="endDate") String endDate) throws ParseException {
//		ModelAndView mav = new ModelAndView();
//		// 絞り込まれた投稿を所得
//		List<Report> report = reportService.narrowContent(startDate, endDate);
//		// 絞り込まれた投稿をセット
//		mav.addObject("contents", report);
//		// 画面遷移先を指定
//		mav.setViewName("/top");
//		return mav;
//	}

	// 投稿の絞り込み ajax
	@GetMapping("/narrow")
	@ResponseBody
	public String narrowContent(@RequestParam(name="startDate") String startDate,
			@RequestParam(name="endDate") String endDate) throws ParseException {

		// 絞り込まれた投稿を取得
		List<Report> report = reportService.narrowContent(startDate, endDate);

		return  gettingJson(report);
	}

    private String gettingJson(List<Report> narrowContent){
        String retVal = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try{
            retVal = objectMapper.writeValueAsString(narrowContent);
        } catch (JsonProcessingException e) {
            System.err.println(e);
        }
        return retVal;
    }

}