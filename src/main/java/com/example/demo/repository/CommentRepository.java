package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Comment;
@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer> {

	 @Query(nativeQuery = true, value = "select * from comment order by id desc limit 1 ")
	List<Comment> findlatest();
}