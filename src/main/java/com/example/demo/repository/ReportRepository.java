package com.example.demo.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;
@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {

	List<Report> findAllByOrderByUpdatedDateDesc();

	List<Report> findByCreatedDateBetween(Date start, Date end);

	 @Query(nativeQuery = true, value = "select * from report order by id desc limit 1 ")
	List<Report> findlatest();

//	 @Query(nativeQuery = true, value = "select * from report where id = Id ")
//	List<Report> findEditContent(Integer Id);

}

