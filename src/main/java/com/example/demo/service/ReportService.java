package com.example.demo.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {

	@Autowired
	ReportRepository reportRepository;

	// レコード全件取得
	public List<Report> findAllReport() {
		// 降順に変更
		return reportRepository.findAllByOrderByUpdatedDateDesc();
	}

	// レコード追加
	public void saveReport(Report report) {
		reportRepository.save(report);
	}

	// レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	// レコード1件取得
	public Report editReport(Integer id) {
		Report report = (Report) reportRepository.findById(id).orElse(null);
		return report;
	}

	// 投稿検索
	public List<Report> narrowContent(String startDate, String endDate) {

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Date start = null;
		try {
			if (startDate != "") {
		       	 startDate += " 00:00:00";
		        } else {
		       	 startDate = "2020-01-01 00:00:00";
		        }
			start = sdFormat.parse(startDate);
		} catch (ParseException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}

        Date end = null;
		try {
	        if (endDate != "") {
	          	 endDate += " 23:59:59";
	           } else {
	          	 endDate = "yyyy-MM-dd HH:mm:ss";
	           }
			end = sdFormat.parse(endDate);
		} catch (ParseException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		List<Report> report = reportRepository.findByCreatedDateBetween(start, end);
		return report;
	}

	// updateddateを更新
	public void updateReport(Report report) {
		reportRepository.save(report);
	}

	// レコード1件取得 （最新データ）
	public List<Report> findSelectedPost() {
		return reportRepository.findlatest();
	}
}