$(document).ready( function(){

	// コメント機能 id付与
	var moji = "note"
		var tmp = document.getElementsByClassName("note") ;

	for(var i=0;i<=tmp.length-1;i++){
		//id追加
		tmp[i].setAttribute("id",moji+i);
	}

	var moji2 = "note_form"
	var tmp2 = document.getElementsByClassName("note_form") ;

	for(var i=0;i<=tmp2.length-1;i++){
		//id追加
		tmp2[i].setAttribute("id",moji2+i);
	}

	var moji3 = "notes"
		var tmp3 = document.getElementsByClassName("notes") ;

	for(var i=0;i<=tmp3.length-1;i++){
		//id追加
		tmp3[i].setAttribute("id",moji3+i);
	}

	var moji4 = "edit"
		var tmp4 = document.getElementsByClassName("edit") ;

	for(var i=0;i<=tmp4.length-1;i++){
		//id追加
		tmp4[i].setAttribute("id",moji4+i);
	}

	var moji5 = "contents"
		var tmp5 = document.getElementsByClassName("contents") ;

	for(var i=0;i<=tmp5.length-1;i++){
		//id追加
		tmp5[i].setAttribute("id",moji5+i);
	}

	var moji6 = "latter_content"
		var tmp6 = document.getElementsByClassName("latter_content") ;

	for(var i=0;i<=tmp6.length-1;i++){
		//id追加
		tmp6[i].setAttribute("id",moji6+i);
	// コメント機能 id付与
	}

});

// 新規投稿
function Post() {

	let content = $(".content").val();

	$.ajax({
		url: "/add",  // リクエストを送信するURLを指定（action属性のurlを抽出）
		type: "POST",  // HTTPメソッドを指定（デフォルトはGET）
		data: {
			"content": content
		},
		dataType: "json"
	})
	.done(function(data) {
		var data_stringify = JSON.stringify(data);
		var data_json = JSON.parse(data_stringify);

		var data_id = data_json[0]["id"];
		var content = data_json[0]["content"];
		var updatedDate = data_json[0]["updatedDate"];
		var date = new Date(updatedDate);
		let correct_date_format = date.getFullYear() + "/" + (date.getMonth()+1) + "/" + date.getDate() +
		" " + date.getHours() + ":"+ date.getMinutes() + ":" + date.getSeconds();

		$(".ajax-display").before(`<div id="${data_id}" style="padding: 20px;"></div>`);

		var ajax_data_id = "#" + data_id;

		$(ajax_data_id).append(`<div>${content}</div>`);  // HTMLを追加
		$(ajax_data_id).append(`<div>${correct_date_format}</div>`);  // HTMLを追加


//		Comment delete form
		$(ajax_data_id).append(`<form action="/delete/${data_id}" method="delete" style="display:inline;">
		<button type="button" onclick="deleteContent('${data_id}')">削除</button></form>`);

//		Comment edit form
		$(ajax_data_id).append(`<div style="display:inline;" id="edit${data_id}"><a action="/edit/${data_id}" style="text-decoration: none;">
		<button type="button" onclick="getEditFunk('${data_id}','${data_id}')">編集</button></a></div>`);

		var notes = "notes" + data_id;

//		Comment display
		$(ajax_data_id).append(`<div id="${notes}"></div>`);

		var note = "note" + data_id;
		var note_form = "note_form" + data_id;

//		Comment form
		$(ajax_data_id).append(`<form action="/comment/${data_id}" method="post" id="${note_form}">
		<input name="reply" size="20" maxlength="200" id="${note}" style="margin: 10px 0;" />
		<button type="button" onclick="return clickFunk('${data_id}','${data_id}','${content}')">
		送信</button></form>`);

		$(".content").val("");  // 入力欄を空にする
	})
	.fail(function(jqXHR, textStatus, errorThrown) {
		alert("error!");  // 通信に失敗した場合の処理
		console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
		console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラー
		console.log("errorThrown    : " + errorThrown.message); // 例外情報
	})

};
// 新規投稿

//削除機能
function deleteContent(id) {

	let delete_url = "/delete/" + id;
	let html_id = "#" + id;

	if(window.confirm('コメントを削除してもよろしいでしょうか？')){ // 確認ダイアログを表示
		$.ajax({
			url: delete_url,  // リクエストを送信するURLを指定（action属性のurlを抽出）
			type: "delete",  // HTTPメソッドを指定（デフォルトはGET）
			data: {
				id: id,
			}
		})
		.done(function() {
			$(html_id).remove();  // HTMLを削除
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			alert("error!");  // 通信に失敗した場合の処理
			console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
			console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラー
			console.log("errorThrown    : " + errorThrown.message); // 例外情報
		})
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}

};
//削除機能

//編集取得機能
function getEditFunk(id, index) {

	let edit_url = "/edit/" + id;

	$.ajax({
		url: edit_url,  // リクエストを送信するURLを指定（action属性のurlを抽出）
		type: "GET",  // HTTPメソッドを指定（デフォルトはGET）
		data: {
			"id": id
		},
		dataType: "json"
	})
	.done(function(data) {
//		var data_stringify = JSON.stringify(data);
//		var data_json = JSON.parse(data_stringify);

		var data_id = data["id"];
		var content = data["content"];

		var contents = "#contents" + index;

		$(contents).remove();  // HTMLを削除

		var latter_content = "#latter_content" +index;
		var content_number = "edit_content" + id;

		$(latter_content).before(`<input type="text" name="content" size="20" maxlength="200" id="${content_number}" value="${content}"/>`);  // HTMLを追加

		let edit_btn = "#edit" + index;

		$(edit_btn).children().remove();  // HTMLを削除

//		Update btn
		$(edit_btn).append(`
		<a action="/updata/${data_id}" method="put">
			<button type="button"  onclick="return updataFunk('${data_id}','${index}')">更新</button>
		</a>`);

	})
	.fail(function(jqXHR, textStatus, errorThrown) {
		alert("error!");  // 通信に失敗した場合の処理
		console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
		console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラー
		console.log("errorThrown    : " + errorThrown.message); // 例外情報
	})

};
// 編集取得機能

//編集更新
function updataFunk(id, index) {

	let content_id = "#edit_content" + id;
	let updata_url = "/updata/" + id;
	let updata_content = $(content_id).val();

	$.ajax({
		url: updata_url,  // リクエストを送信するURLを指定（action属性のurlを抽出）
		type: "PUT",  // HTTPメソッドを指定（デフォルトはGET）
		data: {
			"id": id,
			"content": updata_content
		},
		dataType: "json"
	})
	.done(function(data) {
		var data_stringify = JSON.stringify(data);
		var data_json = JSON.parse(data_stringify);

		var data_id = data_json["id"];
		var content = data_json["content"];
		var updatedDate = data_json["updatedDate"];
		var date = new Date(updatedDate);
		let correct_date_format = date.getFullYear() + "/" + (date.getMonth()+1) + "/" + date.getDate() +
		" " + date.getHours() + ":"+ date.getMinutes() + ":" + date.getSeconds();

		let delete_input = "#edit_content" + id;

		$(delete_input).remove();  // HTMLを削除

		var latter_content = "#latter_content" +index;
		var content_number = "contents" + index;

		$(latter_content).before(`<div id="${content_number}">${content}</div>`);  // HTMLを追加

		$(latter_content).remove();  // HTMLを削除（時間表記）

		var before_time = "#" + content_number;
		var time_id = "latter_content" + index;

		$(before_time).after(`<div id="${time_id}">${correct_date_format}</div>`);  // HTMLを追加（時間表記）

		let edit_btn = "#edit" + index;

		$(edit_btn).children().remove();  // HTMLを削除

//		Edit btn
		$(edit_btn).append(`
		<a action="/updata/${data_id}" method="put">
			<button type="button"  onclick="getEditFunk('${data_id}','${index}')">編集</button>
		</a>`);

	})
	.fail(function(jqXHR, textStatus, errorThrown) {
		alert("error!");  // 通信に失敗した場合の処理
		console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
		console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラー
		console.log("errorThrown    : " + errorThrown.message); // 例外情報
	})

};
// 編集更新

//コメント削除機能
function deleteFunk(id) {

	let delete_url = "/commentDelete/" + id;
	let html_id = "#" + id;

	if(window.confirm('投稿を削除してもよろしいでしょうか？')){ // 確認ダイアログを表示
		$.ajax({
			url: delete_url,  // リクエストを送信するURLを指定（action属性のurlを抽出）
			type: "delete",  // HTTPメソッドを指定（デフォルトはGET）
			data: {
				id: id,
			}
		})
		.done(function() {
			$(html_id).remove();  // HTMLを削除
		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			alert("error!");  // 通信に失敗した場合の処理
			console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
			console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラー
			console.log("errorThrown    : " + errorThrown.message); // 例外情報
		})
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}

};
//コメント削除機能

//コメント追加機能
function clickFunk(index, id, content) {

	let note = "#note" + index;
	let notes = "#notes" + index;
	let reply = $(note).val();
	let comment_url = "/comment/" + id;

	if(window.confirm('コメントを追加してよろしいでしょうか？')){ // 確認ダイアログを表示
		$.ajax({
			url: comment_url,  // リクエストを送信するURLを指定（action属性のurlを抽出）
			type: "POST",  // HTTPメソッドを指定（デフォルトはGET）
			data: {
				"reply": reply,  // 送信データ
				"content": content
			},
			dataType: "json"
		})
		.done(function(data) {
			var data_stringify = JSON.stringify(data);
			var data_json = JSON.parse(data_stringify);

			var data_id = data_json[0]["id"];
			var data_report_id = data_json[0]["report_id"];
			var data_reply = data_json[0]["reply"];

			$(notes).append(`<div class="ajax-coment" id="${data_id}"></div>`);  // HTMLを追加

			var ajax_data_id = "#" + data_id;

			$(ajax_data_id).append(`<div>${data_reply}</div>`);  // HTMLを追加

//			Comment delete form
			$(ajax_data_id).append(`<form action="/commentDelete/${data_id}" method="delete">
			<button type="button" onclick="deleteFunk('${data_id}')">コメント削除</button></form>`);

//			Comment edit form
			$(ajax_data_id).append(`<a href="/commentEdit/${data_id}">
			<button type="button">コメント編集</button></a>`);

			$(note).val("");  // 入力欄を空にする

		})
		.fail(function(jqXHR, textStatus, errorThrown) {
			alert("error!");  // 通信に失敗した場合の処理
			console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
			console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラー
			console.log("errorThrown    : " + errorThrown.message); // 例外情報
		})
		return true; // 「OK」時は送信を実行
	}
	else{ // 「キャンセル」時の処理
		return false; // 送信を中止
	}

};
//コメント追加機能

//絞り込み機能
function narrowContent() {

	let startDate = $(start).val();
	let endDate = $(end).val();

	$.ajax({
		url: "/narrow",  // リクエストを送信するURLを指定（action属性のurlを抽出）
		type: "GET",  // HTTPメソッドを指定（デフォルトはGET）
		data: {
			"startDate": startDate,
			"endDate": endDate
		},
		dataType: "json"
	})
	.done(function(data) {
		var data_stringify = JSON.stringify(data);
		var data_json = JSON.parse(data_stringify);

		var i = Object.keys(data_json).length;

		$(".ajax-display").empty();

		if(i != 0) {
			// 既存のhtml削除
			for(let a = 0; a < i; a++) {
				var data_id = data_json[a]["id"];
				var content = data_json[a]["content"];
				var updatedDate = data_json[a]["updatedDate"];
				var date = new Date(updatedDate);
				let correct_date_format = date.getFullYear() + "/" + (date.getMonth()+1) + "/" + date.getDate() +
				" " + date.getHours() + ":"+ date.getMinutes() + ":" + date.getSeconds();

				$(".ajax-display").append(`<div id="${data_id}" style="padding: 20px;"></div>`);

				var ajax_data_id = "#" + data_id;

				$(ajax_data_id).append(`<div>${content}</div>`);  // HTMLを追加
				$(ajax_data_id).append(`<div>${correct_date_format}</div>`);  // HTMLを追加


//				Comment delete form
				$(ajax_data_id).append(`<form action="/delete/${data_id}" method="delete" style="display:inline;">
				<button type="button" onclick="deleteContent('${data_id}')">削除</button></form>`);

//				Comment edit form
				$(ajax_data_id).append(`<div style="display:inline;"><a href="/edit/${data_id}" style="text-decoration: none;">
				<button type="button">編集</button></a></div>`);
			}
		}
		$(".ajax-display").append(`<div style="padding: 20px;"><a href="/"><button type="button">一覧に戻る</button></a></div>`);

	})
	.fail(function(jqXHR, textStatus, errorThrown) {
		alert("error!");  // 通信に失敗した場合の処理
		console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
		console.log("textStatus     : " + textStatus);    // タイムアウト、パースエラー
		console.log("errorThrown    : " + errorThrown.message); // 例外情報
	})

};
//絞り込み機能